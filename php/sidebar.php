<aside class="sidebar">
  <?php 
    if ($WHERE_AM_I != 'home') {
	?>
      <p><a href="/" class="btn btn-primary">← Accueil</a></p>
	<?php			
    }
  ?>
  <section class="card c-primary">
    <h2 class="card-header"><svg class="icon" alt=""><use xlink:href="#icon-rss"></use></svg> Publications</h2>
    <div class="menu fg-dark">
      <ul>
        <?php
          $items = $pages->getList(1, 6, true);
          foreach ($items as $key) {
            // buildPage function returns a Page-Object
            $page = buildPage($key);
          ?>
          <li>
            <a class="menu-element" href="<?php echo $page->permalink(); ?>"><?php echo $page->title(); ?></a></li>
          </li>
        <?php
          }
        ?>
      </ul>
    </div>
  </section>
  <section class="card c-primary">
    <h2 class="card-header"><svg class="icon icon-folder" alt=""><use xlink:href="#icon-folder"></use></svg> Catégories</h2>
    <div class="menu fg-dark">
      <ul>
        <?php
          $items = getCategories();
          foreach ($items as $category) {
            if (count($category->pages())>0) {
            ?>
            <li><a class="menu-element" href="<?php echo $category->permalink(); ?>"><?php echo  $category->name(); ?> <span class="badge bg-primary m0"><?php echo count($category->pages()); ?></span></a></li>
          <?php }
          }
        ?>
      </ul>
    </div>
  </section>
  <section class="card c-primary">
  <h2 class="card-header"><svg class="icon" alt=""><use xlink:href="#icon-tags"></use></svg> Tags</h2>
  <div class="card-body">
    <ul class="tag-list">
      <?php
        $items = getTags(); 

        foreach ($items as $tag) {
            if (count($tag->pages()) > 1) {
              echo '<li><a href="'.$tag->permalink().'">'.$tag->name().'</a></li>';
            }
        }
      ?>
    </ul>
  </div>
</section>
<p class="align-center"><a href="/rss.xml" class="btn btn-warning">Flux RSS du blog</a></p>
</aside>