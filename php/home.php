<main id="skip">
  <?php Theme::plugins('pageBegin'); ?>
  <h1 class="page-title" id="title-featured">
    Bienvenue sur mon blog
  </h1>
  <section class="mb-1 h-card">
    <picture>
      <source type="image/avif" srcset="<?php echo HTML_PATH_THEME_IMG; ?>/hhghtthrhtr-small.avif" />
      <img
        src="<?php echo HTML_PATH_THEME_IMG; ?>/hhghtthrhtr-small.png"
        alt=""
        class="myavatar u-photo d-none" />
    </picture>
    <p>Bienvenue sur la partie blog du site de <a href="https://kazhnuz.space/" class="p-name u-url">Kazhnuz</a> ! Sur cette partie, j'y poste mes pensées, mes textes, bref tout ce que je fais un peu en vrac (heureusement y'a les tags et les catégories pour s'y retrouver).</p>
  </section>
  <section>
    <ul class="h-feed">
      <?php foreach ($content as $page) : ?>
      <?php if ($page->type() != "sticky") : ?>
        <li class="h-entry">
          <time class="dt-published" datetime="<?php echo $page->date(DATE_ATOM) ?>"><?php echo $page->date('j F Y') ?></time> : « <a class="p-name u-url" href="<?php echo $page->permalink(); ?>" ><?php echo $page->title(); ?></a> »
        </li>
      <?php endif ?>
      <?php endforeach ?>
    </ul>
</section>
<!-- Pagination -->
<?php if (Paginator::numberOfPages() > 1) : ?>
  <nav class="paginator mb-2 mt-1">
    <ul class="pagination flex-that no-pills">

      <!-- Previous button -->
      <?php if (Paginator::showPrev()) : ?>
        <li class="page-item m-0 p-0">
          <a class="page-link m-0" href="<?php echo Paginator::previousPageUrl() ?>" tabindex="-1">← <?php echo $L->get('Previous'); ?></a>
        </li>
      <?php else : ?>
        <li class="page-item m-0 p-0">
          <span class="page-link m-0" tabindex="-1">← <?php echo $L->get('Previous'); ?></span>
        </li>
      <?php endif; ?>

      <!-- Home button -->
      <li class="page-item p-0 m-0">
        <span>Page <?php echo Paginator::currentPage(); ?> sur <?php echo Paginator::numberOfPages(); ?> </span>
      </li>

      <!-- Next button -->
      <?php if (Paginator::showNext()) : ?>
        <li class="page-item m-0 p-0">
          <a class="page-link m-0" href="<?php echo Paginator::nextPageUrl() ?>"><?php echo $L->get('Next'); ?> →</a>
        </li>
      <?php else : ?>
        <li class="page-item m-0 p-0">
          <span class="page-link m-0" tabindex="-1"><?php echo $L->get('Next'); ?> →</span>
        </li>
      <?php endif; ?>

    </ul>
  </nav>
<?php endif ?>
</main>

<?php include(THEME_DIR_PHP.'sidebar.php'); ?>