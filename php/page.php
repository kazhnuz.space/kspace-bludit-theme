<main id="skip">
	<?php Theme::plugins('pageBegin'); ?>
	<article class="article container-article mb-1 h-entry">
		<h1 class="page-title p-name"><?php echo $page->title(); ?></h1>
		<?php if (!$page->isStatic() && !$url->notFound()): ?>
			<div class="flex-that mb-1">
				<time class="author-date dt-published" datetime="<?php echo $page->date(DATE_ATOM) ?>"><i>Le <?php echo $page->date('l j F Y à H:i') ?></i></time>
				<p class="align-right pr-half"><span class="btn btn-small c-secondary"> <?php echo $page->readingTime() ?> </span></p>
			</div>
			
		<?php endif ?>

		<div class="article-entry mb-1 e-content">
			<?php echo $page->content(); ?>
		</div>
		<?php if (!$page->isStatic() && !$url->notFound()): ?>
			<aside class="card card-noheader article-meta">
    		<h2 class="sr-only">Métadonnées de l'article</h2>
				<div class="card-body">
					<div class="author-area p-author h-card">
						<img alt="" src="<?php echo $page->user('profilePicture');?>" srcset="https://secure.gravatar.com/avatar/8a4aed71aeec9f65c9e098d81ff12638?s=240&amp;d=mm&amp;r=g 2x" class="avatar avatar-120 photo u-photo" height="120" width="120" decoding="async">
						<div class="author-metadata">
							<div class="author-pseudo">Écrit par <a href="https://kazhnuz.space" class="u-url p-name"><?php echo $page->user('nickname'); ?></a></div>
							<small class="author-date p-note"><?php echo $page->user('firstname'); ?></small>
						</div>
					</div>

					<p class="mb-0 pb-half p-summary"><?php echo $page->description(); ?></p>

					<div class="article-category">
						<h3 class="sr-only">Tags et catégories</h3>
						<ul class="nolist" aria-labelledby="title-article-taxo-categories">
							<h4 class="sr-only" id="title-article-taxo-categories">Catégories</h4>
							<li>
								<a href="<?php echo $page->categoryPermalink(); ?>" rel="tag directory" class="btn btn-small c-primary p-category mr-half">
									<svg class="icon" alt=""><use xlink:href="#icon-folder"></use></svg>
									&nbsp;<?php echo $page->category(); ?>
								</a>
							</li>
								<?php 
									foreach ($page->tags(true) as $x => $y) {
								?>
									<li>
										<a href="<?php 
											$tagObject = new Tag($x);
											echo $tagObject->permalink(); ?>" rel="tag directory" class="btn btn-small c-secondary p-category mr-half">
											<svg class="icon" alt=""><use xlink:href="#icon-tags"></use></svg>
											&nbsp;<?php echo $y; ?>
										</a>
									</li>
								<?php
										}
								?>
						</ul>      
					</div>
				</div>
			</aside>
		<?php endif ?>
		<?php Theme::plugins('pageEnd'); ?>
	</article>
</main>

<?php include(THEME_DIR_PHP.'sidebar.php'); ?>
