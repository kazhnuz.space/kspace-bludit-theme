<footer>
  <p>
    <a href="https://kobold.cafe/">
      <img src="<?php echo HTML_PATH_THEME_IMG; ?>/koboldcafe.gif" alt="Ce site hébergé par Kobold Cafe">
    </a>
    <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" rel="licence">
      <img src="<?php echo HTML_PATH_THEME_IMG; ?>/cc-by-sa.gif" alt="Ce site est sous Creative Common Attribution - Partage dans les Mêmes Conditions 4.0 International">
    </a>
  </p>
</footer>