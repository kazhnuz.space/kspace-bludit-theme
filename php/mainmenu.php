<nav id="mobile-sidebar" class="bg-dark fg-light sidebar menu hidden">
			<picture>
				<source type="image/avif" srcset="<?php echo HTML_PATH_THEME_IMG; ?>/hhghtthrhtr-small.avif" />
				<img
					src="<?php echo HTML_PATH_THEME_IMG; ?>/hhghtthrhtr-small.png"
					alt=""
					class="myavatar" />
			</picture>
			<h1 class="align-center text-light sr-only">Menu du site</h1>
			<div class="mb-half">
				<div>
					<label for="searchfield" class="sr-only">Formulaire de recherche</label>
					<input class="form-control" id="search-input" type="search" placeholder="Rechercher sur le blog" aria-label="Rechercher sur le blog" value="" name="s">
				</div>
			</div>
			<ul>
				<li>
					<a href="https://kazhnuz.space/" class="menu-item">
						<span>Accueil</span>
					</a>
				</li>
				<li class="item">
					<a href="/" class="menu-item active">
						<span>Blog</span>
					</a>
				</li>
				<li>
					<a href="https://shaarli.kazhnuz.space" class="menu-item">
						<span>Veille informatique</span>
					</a>
				</li>
			</ul>
			<h2>Pages</h2>	
			<ul class="nav">
        <li class="item"><a href="https://kazhnuz.space/about/" class="itemLink">À propos</a></li>
        <li class="item"><a href="https://kazhnuz.space/univers/" class="itemLink">Mes univers</a></li>
        <li class="item"><a href="https://kazhnuz.space/projets/" class="itemLink">Mes projets</a></li>
        <li class="item"><a href="https://kazhnuz.space/fandom/" class="itemLink">Fandoms et shrines</a></li>
        <li class="item"><a href="https://kazhnuz.space/outils/" class="itemLink">Outils et guides</a></li>
        <li class="item"><a href="https://kazhnuz.space/siteroll/" class="itemLink">Siteroll</a></li>
      </ul>
</nav>
				<script>
						function searchNow() {
							var searchURL = "<?php echo Theme::siteUrl(); ?>search/";
							window.open(searchURL + document.getElementById("search-input").value, "_self");
						}
						document.getElementById("search-input").onkeypress = function(e) {
							if (!e) e = window.event;
							var keyCode = e.keyCode || e.which;
							if (keyCode == '13' && document.getElementById("search-input").value !== "") {
								searchNow();
								return false;
							}
						}
					</script>