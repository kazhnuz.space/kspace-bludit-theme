<header id="page-header">
	<div class="logo-area container">
    <h1>
			<a href="<?php echo Theme::siteUrl() ?>"><img src="<?php echo HTML_PATH_THEME_IMG; ?>/logo.png" alt=""/> 
				<span class="sr-only"><?php echo $site->title() ?></span>
			</a>
		</h1>
	</div>
</header>
